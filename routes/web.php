<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\DashboardController;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\DataController;
use App\Http\Controllers\Admin\DashboardAdminController;
// use App\Http\Controllers\Admin\RepositoriAdminController;
use App\Http\Controllers\Admin\DataUserAdminController;
use App\Http\Controllers\Admin\DataKegiatanAdminController;
use App\Http\Controllers\Admin\DataKaryawanAdminController;
use App\Http\Controllers\Admin\DataStatusKegiatanAdminController;
use App\Http\Controllers\Admin\DataInformasiAdminController;
use App\Http\Controllers\Admin\DataLaporanAdminController;
// use App\Http\Controllers\User\DashboardController;
// use App\Http\Controllers\User\RepositoriController;
use App\Http\Controllers\User\DashboardUserController;
// use App\Http\Controllers\PendaftaranController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('auth.login');
});

Route::get('/fcm', [App\Http\Controllers\NotifController::class, 'index']);
Route::get('/send', [App\Http\Controllers\NotifController::class, 'sendNotification']);

Route::get('/login', [AuthController::class, 'index'])->name('login');
Route::post('/login', [AuthController::class, 'login'])->name('login-post');
Route::get('/regist', [AuthController::class, 'regist'])->name('registrasi');
Route::post('registrasi', [AuthController::class, 'registrasi'])->name('registrasi-user');
Route::get('/logout', [AuthController::class, 'logout'])->name('logout');
Route::get('pendaftaran', [PendaftaranController::class, 'index'])->name('pendaftaran');


//middeware
Route::group(['middleware' => ['cekLogin']], function () {

    // Route::get('/dashboard', [DashboardController::class, 'index'])->name('dashboard');
    // Route::get('repositori', [DataController::class, 'index'])->name('repositori');
    // Route::post('tambah_data', [DataController::class, 'tambah'])->name('tambah_data');

    Route::get('upload', [DataController::class, 'file'])->name('file');
    Route::post('upload', [DataController::class, 'upload'])->name('upload');


    Route::group(['middleware' => ['cekAdmin']], function () {
        // Admin
        Route::get('/dashboard_admin', [DashboardAdminController::class, 'index'])->name('dashboard_admin');
        Route::get('/profile_admin', [DashboardAdminController::class, 'profile'])->name('profile.admin');
        Route::put('/ubah.profile.admin/{id}', [DashboardAdminController::class, 'ubahProfile'])->name('ubah.profile.admin');
        Route::resource('user', DataUserAdminController::class);
        Route::resource('karyawan', DataKaryawanAdminController::class);
        Route::resource('kegiatan', DataKegiatanAdminController::class);
        Route::resource('status_kegiatan', DataStatusKegiatanAdminController::class);
        Route::resource('informasi', DataInformasiAdminController::class);
        Route::resource('laporan', DataLaporanAdminController::class);
        Route::post('laporan/cari', [DataLaporanAdminController::class, 'cari'])->name('laporan.cari');
        Route::get('cetak_pdf/{awal}/{akhir}', [DataLaporanAdminController::class, 'cetak_pdf'])->name('cetak.pdf');
    });

    Route::group(['middleware' => ['cekUser']], function () {
        // User
        Route::get('/dashboard', [DashboardUserController::class, 'index'])->name('dashboard');
        Route::get('/pekerja', [DashboardUserController::class, 'pekerja'])->name('pekerja');
        Route::get('/informasi-pekerja', [DashboardUserController::class, 'informasi'])->name('informasi-pekerja');
        Route::get('/agenda', [DashboardUserController::class, 'kegiatan'])->name('agenda');
        Route::get('/cetak-kegiatan', [DashboardUserController::class, 'cetakKegiatan'])->name('cetakKegiatan');
        Route::get('/cetak-kegiatan/{id}', [DashboardUserController::class, 'cetakKegiatanId'])->name('cetakKegiatanId');
        // Route::get('/detail/agenda', [DashboardUserController::class, 'detailAgenda'])->name('detail-agenda');
        Route::get('/profil', [DashboardUserController::class, 'profil'])->name('profil');
        Route::put('/ubah.profile/{id}', [DashboardUserController::class, 'ubahProfile'])->name('ubah.profile');

        //repositori
        Route::get('/repositori', [RepositoriController::class, 'index'])->name('repositori');
        Route::get('/tambah_data', [RepositoriController::class, 'tambah'])->name('tambah');
        Route::post('/insert_data', [RepositoriController::class, 'insert'])->name('insert');
        Route::get('repositori_ubah/{id}', [RepositoriController::class, 'ubah']);
        Route::post('repositori_edit/{id}', [RepositoriController::class, 'edit'])->name('repositori_edit');
        Route::get('repositori_hapus/{id}', [RepositoriController::class, 'hapus']);
    });
});
