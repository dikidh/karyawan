<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Models\User;
use App\Models\Kegiatan;
use App\Models\Karyawan;
use App\Models\Informasi;
use Auth;
use File;
use PDF;

class DashboardUserController extends Controller
{
    public function index(){
        return view('user.dashboard');
    }

    public function kegiatan()
    {
        $kegiatans = Kegiatan::all();
        // dd($kegiatans->);
        return view('user.agenda', compact('kegiatans'));
    }

    public function cetakKegiatan(){
        $kegiatans = Kegiatan::all();
        $pdf = PDF::loadview('user.cetakKegiatan', compact('kegiatans'));
        return $pdf->download('Laporan Data Kegiatan.pdf');
    }

    public function cetakKegiatanId($id){
        $kegiatans = Kegiatan::findOrFail($id);
        $pdf = PDF::loadview('user.cetakKegiatanId', compact('kegiatans'));
        return $pdf->download('Laporan Data Kegiatan.pdf');
    }

    public function informasi()
    {
        $informasis = Informasi::orderBy('created_at', 'asc')->get();
        // $informasis = Informasi::all();
        return view('user.informasi', compact('informasis'));
    }
    
    public function pekerja()
    {
        $pekerja = Karyawan::all();
        return view('user.data_pekerja', compact('pekerja'));
    } 

    public function profil(){
        $user = User::where('id', Auth::user()->id)->first();
        return view('user.profile', compact('user'));
    }

    public function ubahProfile(Request $request, $id){
        // dd($id);
        $data = $request->all();
        // dd($data['foto']);
        $karyawan = User::find($id);
        // dd($karyawan);
        if (isset($data['foto'])) {
            $hapus = 'storage/' . $data['foto'];
            if (File::exists($hapus)) {
                File::delete('storage/app/public/' . $data['foto']);
            }
        }
        if (isset($data['foto'])) {
            $data['foto'] = $request->file('foto')->store('assets/images','public');
        }

        $karyawan->update($data);
        return redirect()->back();
    }
}
