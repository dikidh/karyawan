<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Repositori;
use Auth;

class RepositoriController extends Controller
{
    public function index(){
        $repositori = Repositori::where('user_id', Auth::user()->id)->get();
        // dd($repositori);
        return view('user.repositori.index', compact('repositori'));
    }

    public function tambah(){
        return view('user.repositori.tambah_data');
    }

    public function insert(Request $request){
        $request->validate([
            'status_kerjasama' => 'required',
            'tanggal_awal' => 'required',
            'tanggal_akhir' => 'required',
            'dokumen' => 'required|mimes:pdf,doc,docx,jpg,png,jpeg,csv,xlsx',
            'status_dokumen_kerjasama' => 'required',
            'no_dokumen' => 'required',
            'judul_dokumen' => 'required',
            'deskripsi' => 'required',
            'nama_instansi' => 'required',
            'alamat' => 'required',
            'nama' => 'required',
            'jabatan' => 'required',
            'status_kegiatan' => 'required',
        ]);

        $status_kerjasama = $request->status_kerjasama;
        $tanggal_awal = $request->tanggal_awal;
        $tanggal_akhir = $request->tanggal_akhir;
        $status_dokumen_kerjasama = $request->status_dokumen_kerjasama;
        $no_dokumen = $request->no_dokumen;
        $judul_dokumen = $request->judul_dokumen;
        $deskripsi = $request->deskripsi;
        $nama_instansi = $request->nama_instansi;
        $alamat = $request->alamat;
        $nama = $request->nama;
        $jabatan = $request->jabatan;
        $status_bentuk_kegiatan = $request->status_kegiatan;

        $fileName = time().'.'.$request->dokumen->extension();
        $request->dokumen->move(public_path('uploads'), $fileName);

        $repositori = new Repositori;
        $repositori->user_id = Auth::user()->id;
        $repositori->status_masa_berlaku = $status_kerjasama;
        $repositori->tanggal_awal = $tanggal_awal;
        $repositori->tanggal_akhir = $tanggal_akhir;
        $repositori->dokumen = $fileName;
        $repositori->status_jenis_dokumen = $status_dokumen_kerjasama;
        $repositori->no_dokumen = $no_dokumen;
        $repositori->judul_kerjasama = $judul_dokumen;
        $repositori->deskripsi = $deskripsi;
        $repositori->nama_instansi = $nama_instansi;
        $repositori->alamat = $alamat;
        $repositori->nama = $nama;
        $repositori->jabatan = $jabatan;
        $repositori->status_bentuk_kegiatan = $status_bentuk_kegiatan;
        $repositori->save();
        
        return redirect('repositori')->with('repositori','Data berhasil di tambahkan');
    }    

    public function ubah($id){
        $repositori = Repositori::find($id);
        return view('user.repositori.ubah', compact('repositori'));
    }

    public function edit(Request $request, $id){
        $request->validate([
            'status_kerjasama' => 'required',
            'tanggal_awal' => 'required',
            'tanggal_akhir' => 'required',
            // 'dokumen' => 'required|mimes:pdf,doc,docx,jpg,png,jpeg,csv,xlsx',
            'status_dokumen_kerjasama' => 'required',
            'no_dokumen' => 'required',
            'judul_dokumen' => 'required',
            'deskripsi' => 'required',
            'nama_instansi' => 'required',
            'alamat' => 'required',
            'nama' => 'required',
            'jabatan' => 'required',
            'status_kegiatan' => 'required',
        ]);

        $repositori                             = Repositori::findOrFail($id);
        $repositori->status_masa_berlaku        = $request->status_kerjasama;
        $repositori->tanggal_awal               = $request->tanggal_awal;
        $repositori->tanggal_akhir              = $request->tanggal_akhir;
        $repositori->status_jenis_dokumen       = $request->status_dokumen_kerjasama;
        $repositori->no_dokumen                 = $request->no_dokumen;
        $repositori->judul_kerjasama            = $request->judul_dokumen;
        $repositori->deskripsi                  = $request->deskripsi;
        $repositori->nama_instansi              = $request->nama_instansi;
        $repositori->alamat                     = $request->alamat;
        $repositori->nama                       = $request->nama;
        $repositori->jabatan                    = $request->jabatan;
        $repositori->status_bentuk_kegiatan     = $request->status_kegiatan;

        if ($request->hasfile('dokumen')) {
            $extension = $request->dokumen->getClientOriginalExtension();
            $fileName = time().'.'.$request->dokumen->extension();
            $request->dokumen->move(public_path('uploads'), $fileName);
            $produk->update(['dokumen'=> $fileName]);
        }

        $repositori->save();

        return redirect('repositori')->with('repositori','Daata berhasil di ubah');
    }

    public function hapus($id){
        $hapus = Repositori::findOrFail($id)->delete();
        return back()->with('repositori','Daa berhasil di hapus');
    }
}
