<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Auth;

class AuthController extends Controller
{
    public function index()
    {
        return view('auth.login');
    }

    public function login(Request $request)
    {
        $request->validate([
            'username' => 'required',
            'password' => 'required'
        ]);

        $username = $request->username;
        $password = $request->password;

        $user = User::where('email', $username)->first();
        if ($user) {
            if (auth()->attempt(['email' => $username, 'password' => $password])) {
                if (auth()->user()->status == 1) {
                    if (auth()->user()->role_user == 1 || auth()->user()->role_user == 2) {
                        session(['berhasil_login' => true]);
                        return redirect('dashboard_admin');
                    } else if (auth()->user()->role_user == 3) {
                        session(['berhasil_login' => true]);
                        return redirect('dashboard');
                    }
                } else {
                    return redirect('login')->with('login', 'Akun belum di aktifasi');
                }
            } else {
                return redirect('login')->with('login', 'Login gagal! Password tidak sesuai');
            }
        } else {
            return redirect('login')->with('login', 'Login gagal! Username tidak ditemukan');
        }
    }
    public function regist()
    {
        return view('auth.registrasi');
    }
    public function registrasi(Request $request)
    {
        $request->validate([
            'nama' => ['required'],
            'email' => ['required', 'unique:users'],
            'password' => 'required|min:6|same:password-confirm',
            'password-confirm' => 'required|same:password',
        ]);

        User::create([
            'name' => $request->nama,
            'email' => $request->email,
            'password' => bcrypt($request->password),
            'role_user' => 3,
            'status' => 1,
        ]);
        return redirect('login')->with('regist', 'Akun berhasil di daftarkan');
    }

    public function logout(Request $request)
    {
        auth()->logout();
        $request->session()->invalidate();
        $request->session()->regenerateToken();
        return redirect('login')->with('logout', 'Anda berhasil logout');
    }
}
