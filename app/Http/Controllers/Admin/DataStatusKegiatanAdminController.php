<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Models\StatusKegiatan;
use App\Models\Karyawan;
use App\Models\User;

class DataStatusKegiatanAdminController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $status_kegiatans = StatusKegiatan::all();
        $karyawans = Karyawan::all();
        return view('admin.data_status_kegiatan.index', compact('status_kegiatans','karyawans'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'status_kegiatan' => 'required|string'
        ]);
        $data = $request->all();
        StatusKegiatan::create($data);

        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = $request->all();
        
        $status_kegiatan = StatusKegiatan::findOrFail($id);
        $status_kegiatan->update($data);
        toast()->success('Data Berhasil Di Ubah');
        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $status_kegiatan = StatusKegiatan::findOrFail($id);
        $status_kegiatan->delete();
        toast()->success('Data Berhsail Di Hapus');
        return redirect()->back();
    }
}
