<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Models\Karyawan;
use App\Models\Kegiatan;
use App\Models\Informasi;
use App\Models\User;
use Auth;
use File;

class DashboardAdminController extends Controller
{
    public function index(){
        $user = User::all();
        $karyawan = Karyawan::all();
        $informasi = Informasi::all();
        $kegiatan = Kegiatan::all();
        return view('admin.dashboard', compact('user','karyawan','informasi','kegiatan'));
    }

    public function profile(){
        $user = User::where('id', Auth::user()->id)->first();
        // dd($user);
        return view('admin.profile', compact('user'));
    }

    public function ubahProfile(Request $request, $id){
        // dd($id);
        $data = $request->all();
        // dd($data['foto']);
        $karyawan = User::find($id);
        // dd($karyawan);
        if (isset($data['foto'])) {
            $hapus = 'storage/' . $data['foto'];
            if (File::exists($hapus)) {
                File::delete('storage/app/public/' . $data['foto']);
            }
        }
        if (isset($data['foto'])) {
            $data['foto'] = $request->file('foto')->store('assets/images','public');
        }

        $karyawan->update($data);
        return redirect()->back();
    }
}
