<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Http\Support;

use App\Http\Requests\Admin\UserCreateRequest;
use App\Http\Requests\Admin\UserUpdateRequest;
use App\Models\User;

use File;

class DataUserAdminController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = User::all();
        return view('admin.data_user.index', compact('user'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // dd($request->all());
        $request->validate([
            'name' => 'required','string',
            'email' => 'required','email',
            'password' => 'required','same:password-konfirmasi','min:6',
            'password-konfirmasi' => 'required','smae:password','min:6',
            'role_user' => 'required','integer',
            'foto' => 'required|image'
        ]);

        // $user = new User();
        // $user->name = $request->name;
        // $user->email  = $request->email;
        // $user->password =  bcrypt($request->password);
        // $user->role_user = 2;
        // $user->status = 1;
        // $user->save();
        $data = $request->all();
        $data['password'] = bcrypt($data['password']);
        $data['foto'] = $request->file('foto')->store('assets/images','public');
        // dd($data);
        User::create($data);

        toast()->success('Data Berhasil Di Tambahkan');
        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return abort(404);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data_user = $request->all();
        $get_poto = User::where('id', $id)->first();

        if (isset($data_user['foto'])) {
            $data = 'storage/' . $get_poto['foto'];
            if (File::exists($data)) {
                File::delete($data);
            }
            else{
                File::delete('storage/app/public/' .$get_poto);
            }
        }

        if (isset($data_user['foto'])) {
            $data_user['foto'] = $request->file('foto')->store('assets/images','public');
        }

        $user = User::findOrFail($id);
        $user->update($data_user);

        toast()->success('Data Berhasil Di Ubah');
        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data = User::findOrFail($id);
        $data->delete();
        $path_poto = $data['foto'];

        $data = 'storage/' . $path_poto;
        if (File::exists($data)) {
            File::delete($data);
        }else{
            File::delete('storage/app/public/' . $path_poto);
        }

        toast()->success('Data Berhasil Di Hapus');
        return redirect()->back();
    }
}
