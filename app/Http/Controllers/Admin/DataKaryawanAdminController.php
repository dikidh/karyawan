<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Models\Karyawan;
use App\Models\User;


class DataKaryawanAdminController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = User::where('role_user', 2)->get();
        $karyawans = Karyawan::all();
        return view('admin.data_karyawan.index', compact('karyawans','users'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // dd($request->all());
        $request->validate([
            'users_id' => 'required',
            'posisi' =>  'required',
            'alamat' =>  'required',
            'no_telepon' => 'required|max:13',
            'jenis_kelamin' => 'required'
        ]);
        $data = $request->all();
        $user = Karyawan::where('users_id', $data['users_id'])->first();
        // dd($data['users_id']);
        if ($user) {
            return redirect()->back()->with('karyawan','User Sudah terdaftar');
        }
        Karyawan::create($data);
        // $karyawan = new Karyawan();
        // $karyawan->users_id = $request->users_id;
        // $karyawan->posisi = $request->posisi;
        // $karyawan->alamat = $request->alamat;
        // $karyawan->no_telepon = $request->no_telepon;
        // $karyawan->jenis_kelamin = $request->jenis_kelamin;
        // $karyawan->save();
        toast()->success('Data Berhasil D Simpan');
        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return abort(404);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // dd($request->all());
        $request->validate([
            'posisi' =>  'required',
            'alamat' =>  'required',
            'no_telepon' => 'required|max:13',
            'jenis_kelamin' => 'required'
        ]);

        $data = $request->all();
        $karyawan = Karyawan::findOrFail($id);
        $karyawan->update($data);

        toast()->success('Data Berhasil DI Update');
        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $karyawan = Karyawan::findOrFail($id);
        $karyawan->delete();

        toast()->success('Data Berhasil DI Hapus');
        return redirect()->back();
    }
}
