<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Models\Kegiatan;
use App\Models\Karyawan;
use App\Models\StatusKegiatan;
use App\Models\User;

class DataKegiatanAdminController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $kegiatans = Kegiatan::all();
        // dd($kegiatans);
        // $users = User::where('role_user', '<>', 1)->get();
        $users = Karyawan::all();
        $status_kegiatans = StatusKegiatan::all();
        return view('admin.data_kegiatan.index', compact('kegiatans','users','status_kegiatans'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // dd($request->all());
        $request->validate([
            'nama_kegiatan' => 'required',
            'karyawan_id' => 'required',
            'status_kegiatan_id' => 'required',
            'tanggal' => 'required',
            'hari' => 'required',
            'jam' => 'required',
            'keterangan' => 'required'
        ]);

        $data = $request->all();
        // dd($data);
        Kegiatan::create($data);
        toast()->success('Data Berhasil Di Simpan');
        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'nama_kegiatan' => 'required',
            'karyawan_id' => 'required',
            'status_kegiatan_id' => 'required',
            'tanggal' => 'required',
            'hari' => 'required',
            'jam' => 'required',
            'keterangan' => 'required'
        ]);

        $data = $request->all();
        $kegiatan = Kegiatan::find($id);
        $kegiatan->update($data);
        toast()->success('Data Berhasil Di Ubah');
        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        // dd($id);
        $kegiatan = Kegiatan::findOrFail($id);
        $kegiatan->delete();
        toast()->success('Data Berhasil Di Hapus');
        return redirect()->back();
    }
}
