<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use PDF;

use App\Models\Kegiatan;

class DataLaporanAdminController extends Controller
{
    public function index(){
        return view('admin.laporan.index');
    }

    public function cari(Request $request){
        // dd($request->all());
        $request->validate([
            'tanggal_awal' => 'required',
            'tanggal_akhir' => 'required'
        ]);

        $awal = $request->tanggal_awal;
        $akhir = $request->tanggal_akhir;

        $kegiatans = Kegiatan::whereBetween('tanggal', [$awal, $akhir])->get();
        // dd($kegiatans);

        return view('admin.laporan.filter', compact('kegiatans', 'awal','akhir'));
    }

    public function cetak_pdf($dari,$sampai){
        // dd($dari,$sampai);
        $kegiatans = Kegiatan::whereBetween('tanggal', [$dari, $sampai])->get();
        $pdf = PDF::loadview('admin.laporan.cetak_laporan', compact('kegiatans','dari','sampai'));
        return $pdf->download('Laporan.pdf');
        // $pdf = PDF::loadview('admin.laporan.cetak_laporan', compact('siswa'));
        // return $pdf->download('Laporan-pendaftaran.pdf');
        // return view('admin.laporan.cetak_laporan', compact('kegiatans','dari','sampai'));
    }
}
