<?php

namespace App\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;

class KaryawanUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
             'users_id' => [
                'required'
            ],
            'posisi' => [
                'required'
            ],
            'alamat' => [
                'required'
            ],
            'no_telepon' => [
                'requried'
            ],
            'jenis_kelamin' => [
                'required'
            ]
        ];
    }
}
