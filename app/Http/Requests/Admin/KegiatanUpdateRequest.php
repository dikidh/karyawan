<?php

namespace App\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;

class KegiatanUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'nama_kegiatan' => ['required'],
            'karyawan_id' => ['required'],
            'status_kegiatan' => ['required'],
            'hari' => ['required'],
            'tanggal' => ['required'],
            'jam' => ['required'],
            'keterangan' => ['required'],
        ];
    }
}
