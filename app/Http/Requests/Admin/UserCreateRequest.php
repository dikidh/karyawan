<?php

namespace App\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;

class UserCreateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return false;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => [
                'required','string'
            ],
            'email' => [
                'required','email'
            ],
            'password' => [
                'required','same:password-konfimasi','min:6'
            ],
            'password-konfimasi' => [
                'required','smae:password','min:6'
            ],
            'role_user' => [
                'required','integer'
            ]
        ];
    }
}
