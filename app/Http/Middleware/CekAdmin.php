<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class CekAdmin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure(\Illuminate\Http\Request): (\Illuminate\Http\Response|\Illuminate\Http\RedirectResponse)  $next
     * @return \Illuminate\Http\Response|\Illuminate\Http\RedirectResponse
     */
    public function handle(Request $request, Closure $next)
    {


        // if (!session('berhasil_login')) {
            // return redirect('login')->with('login','Anda belum memiliki akun');
        // }
        // return $next($request);

        if (Auth::user()->role_user == 1 || Auth::user()->role_user == 2) {
            // return redirect('dashboard_admin');
            return $next($request);
        }
        return redirect('login');

    }
}
