<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Pendaftaran extends Model
{
    use HasFactory;

    protected $table = 'tb_pendaftaran';

    protected $fillable = [
        'nidn',
        'nama',
        'jenis_kelamin',
        'tempat_lahir',
        'tanggal_lahir',
        'agama',
        'alamat',
        'no_telepon',
        'upload_kk',
        'upload_akte',
        'upload_ktp_ortu',
        // 'bukti_pembayaran',
        // 'status_bukti_bayar',
    ];

    public function user(){
        // return $this->
    }
}
