<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Kegiatan extends Model
{
    use HasFactory;

    protected $fillable = [
        'nama_kegiatan',
        'karyawan_id',
        'status_kegiatan_id',
        'hari',
        'tanggal',
        'jam',
        'keterangan',
    ];

    public function status_kegiatan(){
        return $this->belongsTo('App\Models\StatusKegiatan', 'status_kegiatan_id','id');
    }

    public function karyawan(){
        return $this->belongsTo('App\Models\Karyawan', 'karyawan_id','id');
    }
}
