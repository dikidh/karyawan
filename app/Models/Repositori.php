<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Repositori extends Model
{
    use HasFactory;

    protected $table = 'repositori';

    protected $fillable = [
        'user_id',
        'status_masa_berlaku',
        'tanggal_awal',
        'tanggal_akhir',
        'dokumen',
        'status_jenis_dokumen',
        'no_dokumen',
        'juduk_kerjasama',
        'deskripsi',
        'nama_instansi',
        'alamat',
        'nama',
        'jabatan',
        'status_bentuk_kegiatan'
    ];
}
