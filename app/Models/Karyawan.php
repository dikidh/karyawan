<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Karyawan extends Model
{
    use HasFactory;

    protected $table = 'karyawans';

    protected $primaryKey = 'id';

    protected $fillable = [
        'users_id',
        'posisi',
        'alamat',
        'no_telepon',
        'jenis_kelamin',
    ];

    public function user(){
        return $this->belongsTo('App\Models\User', 'users_id','id');
    }

    public function kegiatan(){
        return $this->hasOne('App\Models\Kegiatan', 'karyawan_id','id');
    }
}
