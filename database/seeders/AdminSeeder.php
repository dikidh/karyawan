<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\User;

class AdminSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\Models\User::create([
            'name' => 'Haruka Ozora',
            'email' => 'harukaozora@gmail.com',
            'password' => bcrypt('password'),
            'role_user' => '1',
            'status' => '1',
            'foto' => ''
        ]);
    }
}
