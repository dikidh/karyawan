<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class StatusKegiatanSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $status = [
            [
                'status' => 'Selesai',
            ],
            [
                'status' => 'Sedang Berjalan',
            ],
            [
                'status' => 'Belum Mulai',
            ],
        ];
        foreach ($status as $value) {
            \App\Models\StatusKegiatan::create([
                'status_kegiatan' => $value['status'],
            ]);
        }
    }
}
