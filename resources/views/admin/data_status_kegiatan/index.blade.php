@extends('template_admin.app')

@section('title', 'Data Status Kegiatan')

@section('content')
    <section class="section">
        <div class="section-header">
            <h1>Data Status Kegiatan</h1>
        </div>
        <div class="section-body">
            <div class="card">
                <div class="card-body">
                    <a href="" class="btn btn-primary" data-toggle="modal" data-target="#exampleModalTambah">Tambah Data</a>
                    <hr>
                    @if (session('status_kegiatan'))
                        <div class="alert alert-success alert-dismissible">
                            <button type="button" class="close" data-dismiss="alert">&times;</button>
                            <strong>{{ session('status_kegiatan') }}</strong>
                        </div>
                    @endif
                    <table class="table table-bordered table-striped" id="myTable">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Status Kegiatan</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @if (!empty($status_kegiatans))
                                @foreach ($status_kegiatans as $no => $item)
                                    <tr>
                                        <td>{{ $no + 1 }}</td>
                                        <td>{{ $item->status_kegiatan }}</td>
                                        <td>
                                            <form action="{{ route('status_kegiatan.destroy', $item->id) }}"
                                                method="post">
                                                @method('delete')
                                                @csrf
                                                <a href="#" data-toggle="modal"
                                                    data-target="#exampleModalEdit{{ $item->id }}"
                                                    class="btn btn-sm btn-info"><i class="fas fa-edit"></i></a>
                                                <button type="submit" class="btn btn-danger btn-sm"><i
                                                        class="fas fa-trash"></i></button>
                                            </form>
                                        </td>
                                    </tr>
                                @endforeach
                            @endif
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </section>

    <!-- Button trigger modal -->
    {{-- <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal">
        Launch demo modal
    </button> --}}

    <!-- Modal Tambah-->
    <div class="modal fade" id="exampleModalTambah" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
        aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Tambah Data</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form action="{{ route('status_kegiatan.store') }}" method="post">
                        @csrf
                        <div class="form-group">
                            <label for="">Status Kegiatan</label>
                            <input type="text" class="form-control @error('status_kegiatan') is-invalid @enderror"
                                name="status_kegiatan" placeholder="Masukan Status Kegiatan">
                            @error('status_kegiatan')
                                <small class="text-danger">{{ $message }}</small>
                            @enderror
                        </div>
                        <div class="float-right">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                            <button type="submit" class="btn btn-primary">Simpan</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <!-- Modal Edit-->
    @foreach ($status_kegiatans as $item)
        <div class="modal fade" id="exampleModalEdit{{ $item->id }}" tabindex="-1" role="dialog"
            aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Ubah Data</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <form action="{{ route('status_kegiatan.update', $item->id) }}" method="post">
                            @method('PUT')
                            @csrf
                            <div class="form-group">
                                <label for="">Nama</label>
                                <input type="text" class="form-control" name="status_kegiatan" placeholder="Masukan Nama"
                                    value="{{ $item->status_kegiatan ?? '' }}">
                            </div>
                            <div class="float-right">
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                                <button type="submit" class="btn btn-primary">Simpan</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    @endforeach
@endsection
