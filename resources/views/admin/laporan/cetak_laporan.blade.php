<!doctype html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.1/dist/css/bootstrap.min.css"
        integrity="sha384-zCbKRCUGaJDkqS1kPbPd7TveP5iyJE0EjAuZQTgFLD2ylzuqKfdKlfG/eSrtxUkn" crossorigin="anonymous">

    <title>Cetak Laporan</title>
    <style>
        th {
            font-size: 12px;
        }

        td {
            font-size: 10px;
        }
    </style>
</head>

<body>

    <div class="row">
        {{-- <div class="col-md-12 text-center">
            <img src="{{ url('../image/logo_250.png') }}" alt="" width="100">
        </div> --}}
        <div class="col-md-12 mb-4 text-center">
            <div class="row text-center">
                <div class="col-md-12">
                    <img src="{{ public_path('/image/logo_250.png') }}" alt="" width="100">
                    <br>
                    {{-- </div> --}}
                    {{-- <div class="col-md-9 text-center"> --}}
                    <h5 class="font-weight-bold">CV SAMUDERA BIRU NUSANTARA</h5>
                    <small>Jl Gatot subroto Royal Living Blok RA No 7, Pondok Jaya Kec Sepatan Tangerang , Banten
                        15520</small>
                </div>
            </div>
        </div>

        <div class="col-md-12 mb-4">
            <div>
                <strong>Laporan Pertanggal</strong>
            </div>
            <div>
                <small class="font-weight-bold">{{ $dari }} - {{ $sampai }}</small>
            </div>
        </div>

        <div class="col-md-12">
            <table class="table table-bordered table-striped">
                <thead>
                    <tr>
                        <th>No</th>
                        <th>Nama Kegiatan</th>
                        <th>Karyawan</th>
                        <th>Status Kegiatan</th>
                        <th>Hari & Tanggal</th>
                        <th>Jam</th>
                        <th>Keterangan</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($kegiatans as $no => $item)
                        <tr>
                            <td>{{ $no + 1 }}</td>
                            <td>{{ $item->nama_kegiatan }}</td>
                            <td>{{ $item->karyawan->user->name  ?? ''}}</td>
                            <td>{{ $item->status_kegiatan->status_kegiatan }}</td>
                            <td>{{ $item->hari }} {{ $item->tanggal }}</td>
                            <td>{{ $item->jam }}</td>
                            <td>{{ $item->keterangan }}</td>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>

    <!-- Optional JavaScript; choose one of the two! -->

    <!-- Option 1: jQuery and Bootstrap Bundle (includes Popper) -->
    <script src="https://cdn.jsdelivr.net/npm/jquery@3.5.1/dist/jquery.slim.min.js"
        integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous">
    </script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.1/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-fQybjgWLrvvRgtW6bFlB7jaZrFsaBXjsOMm/tB9LTS58ONXgqbR9W8oWht/amnpF" crossorigin="anonymous">
    </script>

    <!-- Option 2: Separate Popper and Bootstrap JS -->
    <!--
    <script src="https://cdn.jsdelivr.net/npm/jquery@3.5.1/dist/jquery.slim.min.js"
        integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous">
    </script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js"
        integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous">
    </script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.1/dist/js/bootstrap.min.js"
        integrity="sha384-VHvPCCyXqtD5DqJeNxl2dtTyhF78xXNXdkwX1CZeRusQfRKp+tA7hAShOK/B/fQ2" crossorigin="anonymous">
    </script>
    -->
</body>

</html>
