@extends('template_admin.app')

@section('title', 'Data Laporan')

@section('content')
    <section class="section">
        <div class="section-header">
            <h1>Data Laporan</h1>
            </h1>
        </div>
        <div class="section-body">
            <div class="card">
                <div class="row">
                    <div class="col-md-6 offset-3">
                        <div class="card-body">
                            @if (session('laporan'))
                                <div class="alert alert-success alert-dismissible">
                                    <button type="button" class="close" data-dismiss="alert">&times;</button>
                                    <strong>{{ session('laporan') }}</strong>
                                </div>
                            @endif
                            <form action="{{ route('laporan.cari') }}" method="post">
                                @csrf
                                <div class="form-group">
                                    <label for="">Tanggal Awal</label>
                                    <input type="date" class="form-control @error('tanggal_awal') is-invalid @enderror"
                                        name="tanggal_awal" required>
                                </div>
                                <div class="form-group">
                                    <label for="">Tanggal Akhir</label>
                                    <input type="date" class="form-control @error('tanggal_akhir') is-invalid @enderror"
                                        name="tanggal_akhir" required>
                                </div>
                                <button type="submit" class="btn btn-success">Cari</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
