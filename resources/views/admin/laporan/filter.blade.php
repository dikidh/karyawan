@extends('template_admin.app')

@section('title', 'Data Laporan')

@section('content')
    <section class="section">
        <div class="section-header">
            <h1>Data Laporan</h1>
            </h1>
        </div>
        <div class="section-body">
            <div class="card">
                <div class="row">
                    <div class="col-md-6 offset-3">
                        <div class="card-body">
                            @if (session('laporan'))
                                <div class="alert alert-success alert-dismissible">
                                    <button type="button" class="close" data-dismiss="alert">&times;</button>
                                    <strong>{{ session('laporan') }}</strong>
                                </div>
                            @endif
                            <form action="{{ route('laporan.cari') }}" method="post">
                                @csrf
                                <div class="form-group">
                                    <label for="">Tanggal Awal</label>
                                    <input type="date" class="form-control @error('tanggal_awal') is-invalid @enderror"
                                        name="tanggal_awal" required>
                                </div>
                                <div class="form-group">
                                    <label for="">Tanggal Akhir</label>
                                    <input type="date" class="form-control @error('tanggal_akhir') is-invalid @enderror"
                                        name="tanggal_akhir" required>
                                </div>
                                <button type="submit" class="btn btn-success">Cari</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12 mb-3">
                    <div class="row">
                        <div class="col-md-6">
                            <strong>Laporan Pertanggal</strong><br>
                            <small class="font-weight-bold">{{ $awal }} - {{ $akhir }}</small>
                        </div>
                        <div class="col-md-6 text-right">
                            @if (count($kegiatans) > 0)
                                <a href="{{ url('cetak_pdf') }}/{{ $awal }}/{{ $akhir }}"
                                    class="btn btn-danger"><i class="fas fa-file"></i> Cetak
                                    Pdf</a>
                            @endif
                        </div>
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-body">
                            <table class="table table-bordered table-striped" id="myTable">
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>Nama Kegiatan</th>
                                        <th>Karyawan</th>
                                        <th>Status Kegiatan</th>
                                        <th>Hari & Tanggal</th>
                                        <th>Jam</th>
                                        <th>Keterangan</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @if (count($kegiatans) == 0)
                                        <div class="alert alert-danger">Data Tidak Di Temukan</div>
                                        {{-- @endif --}}
                                    @elseif (!empty($kegiatans))
                                        @foreach ($kegiatans as $no => $item)
                                            <tr>
                                                <td>{{ $no + 1 }}</td>
                                                <td>{{ $item->nama_kegiatan }}</td>
                                                <td>{{ $item->karyawan_id }}</td>
                                                <td>{{ $item->status_kegiatan->status_kegiatan }}</td>
                                                <td>{{ $item->hari }} {{ $item->tanggal }}</td>
                                                <td>{{ $item->jam }}</td>
                                                <td>{{ $item->keterangan }}</td>
                                        @endforeach
                                        {{-- @elseif(count($kegiatans) == 0) --}}
                                        {{-- <div class="alert alert-danger">Data Kosong</div> --}}
                                    @endif
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
