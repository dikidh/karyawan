@extends('template_admin.app')

@section('title', 'Data User')

@section('content')
    <section class="section">
        <div class="section-header">
            <h1>Data User</h1>
        </div>
        <div class="section-body">
            <div class="card">
                <div class="card-body">
                    <hr>
                    @if (session('repositori'))
                        <div class="alert alert-success alert-dismissible">
                            <button type="button" class="close" data-dismiss="alert">&times;</button>
                            <strong>{{ session('repositori') }}</strong>
                        </div>
                    @endif
                    <table class="table table-bordered table-striped" id="myTable">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Foto</th>
                                <th>Nama</th>
                                <th>Email</th>
                                <th>Role User</th>
                                <th>Status User</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @if (!empty($user))
                                @foreach ($user as $no => $item)
                                    <tr>
                                        <td>{{ $no + 1 }}</td>
                                        <td>
                                            {{-- @if ($item->foto) --}}
                                            <img src="{{ url(Storage::url($item->foto)) }}" alt="" class=""
                                                width="100">
                                            {{-- @endif --}}
                                        </td>
                                        <td>{{ $item->name }}</td>
                                        <td>{{ $item->email }}</td>
                                        <td>
                                            @if ($item->role_user == 1)
                                                <small class="badge badge-secondary">Manager</small>
                                            @elseif($item->role_user == 2)
                                                <small class="badge badge-secondary">Admin</small>
                                            @else
                                                <small class="badge badge-secondary">Pekerja</small>
                                            @endif
                                        </td>
                                        <td>
                                            @if ($item->status != 1)
                                                <small class="badge badge-danger">Tidak Aktif</small>
                                            @else
                                                <small class="badge badge-success">Aktif</small>
                                            @endif
                                        </td>
                                        <td>
                                            @if ($item->id != auth()->user()->id)
                                                <form action="{{ route('user.destroy', $item->id) }}" method="post">
                                                    @method('DELETE')
                                                    @csrf
                                                    <a href="#" data-toggle="modal"
                                                        data-target="#exampleModalEdit{{ $item->id }}"
                                                        class="btn btn-sm btn-info"><i class="fas fa-edit"></i></a>
                                                    <button type="submit" class="btn btn-danger btn-sm"><i
                                                            class="fas fa-trash"></i></button>
                                                </form>
                                            @endif
                                        </td>
                                    </tr>
                                @endforeach
                            @endif
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </section>

    <!-- Button trigger modal -->
    {{-- <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal">
        Launch demo modal
    </button> --}}

    <!-- Modal Tambah-->
    <div class="modal fade" id="exampleModalTambah" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
        aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Tambah Data</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form action="{{ route('user.store') }}" method="post" enctype="multipart/form-data">
                        @csrf
                        <div class="form-group">
                            <label for="">Nama</label>
                            <input type="text" class="form-control @error('name') is-invalid @enderror" name="name"
                                placeholder="Masukan Nama">
                            @error('name')
                                <small class="text-danger">{{ $message }}</small>
                            @enderror
                        </div>
                        <div class="form-group">
                            <label for="">Email</label>
                            <input type="email" class="form-control " name="email" placeholder="Masukan Email">
                            @error('email')
                                <small class="text-danger">{{ $message }}</small>
                            @enderror
                        </div>
                        <div class="form-group">
                            <label for="">Password</label>
                            <input type="password" class="form-control" name="password" placeholder="Masukan Password">
                            @error('password')
                                <small class="text-danger">{{ $message }}</small>
                            @enderror
                        </div>
                        <div class="form-group">
                            <label for="">Konfirmasi Password</label>
                            <input type="password" class="form-control" name="password-konfirmasi"
                                placeholder="Masukan Konfirmasi Password">
                            @error('password-konfirmasi')
                                <small class="text-danger">{{ $message }}</small>
                            @enderror
                        </div>
                        <div class="form-group">
                            <label for="">Role User</label>
                            <select name="role_user" id="" class="form-control">
                                <option value="">-- Pilihan --</option>
                                <option value="1">Admin</option>
                                <option value="2">Pekerja</option>
                            </select>
                            @error('role_user')
                                <small class="text-danger">{{ $message }}</small>
                            @enderror
                        </div>
                        <div class="form-group">
                            <label for="">Status User</label>
                            <select name="status" id="" class="form-control">
                                <option value="">-- Pilihan --</option>
                                <option value="1">Aktive</option>
                                <option value="2">Tidak Aktif</option>
                            </select>
                            @error('status')
                                <small class="text-danger">{{ $message }}</small>
                            @enderror
                        </div>
                        <div class="form-group">
                            <label for="">Foto</label>
                            <input type="file" class="form-control" name="foto">
                            @error('foto')
                                <small class="text-danger">{{ $message }}</small>
                            @enderror
                        </div>
                        <div class="float-right">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Kembali</button>
                            <button type="submit" class="btn btn-primary">Simpan</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <!-- Modal Edit-->
    @foreach ($user as $item)
        <div class="modal fade" id="exampleModalEdit{{ $item->id }}" tabindex="-1" role="dialog"
            aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Ubah Data</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <form action="{{ route('user.update', $item->id) }}" method="post"
                            enctype="multipart/form-data">
                            @method('PUT')
                            @csrf
                            <div class="form-group">
                                <label for="">Nama</label>
                                <input type="text" class="form-control" name="name" placeholder="Masukan Nama"
                                    value="{{ $item->name ?? '' }}">
                                @error('name')
                                    <small class="text-danger">{{ $message }}</small>
                                @enderror
                            </div>
                            <div class="form-group">
                                <label for="">Email</label>
                                <input type="email" class="form-control" name="email" placeholder="Masukan Email"
                                    value="{{ $item->email }}">
                                @error('email')
                                    <small class="text-danger">{{ $message }}</small>
                                @enderror
                            </div>
                            {{-- <div class="form-group">
                                <label for="">Password</label>
                                <input type="password" class="form-control" name="password"
                                    placeholder="Masukan Password">
                                @error('password')
                                    <small class="text-danger">{{ $message }}</small>
                                @enderror
                            </div>
                            <div class="form-group">
                                <label for="">Konfirmasi Password</label>
                                <input type="password" class="form-control" name="password-konfirmasi"
                                    placeholder="Masukan Konfirmasi Password">
                                @error('password-konfirmasi')
                                    <small class="text-danger">{{ $message }}</small>
                                @enderror
                            </div> --}}
                            <div class="form-group">
                                <label for="">Role User</label>
                                <select name="role_user" id="" class="form-control">
                                    <option value="2" {{ $item->role_user = 2 ? 'selected' : '' }}>Admin</option>
                                    <option value="3" {{ $item->role_user = 3 ? 'selected' : '' }}>Pekerja</option>
                                </select>
                                @error('role_user')
                                    <small class="text-danger">{{ $message }}</small>
                                @enderror
                            </div>
                            <div class="form-group">
                                <label for="">Status User</label>
                                <select name="status" id="" class="form-control">
                                    <option value="1" {{ $item->status == 1 ? 'selected' : '' }}>Aktif</option>
                                    <option value="2" {{ $item->status == 2 ? 'selected' : '' }}>Tidak Aktif
                                    </option>
                                </select>
                                @error('status')
                                    <small class="text-danger">{{ $message }}</small>
                                @enderror
                            </div>
                            <div class="form-group">
                                <label for="">Foto</label>
                                <input type="file" class="form-control" name="foto">
                                <small class="text-dark"><i>Biarkan jika foto tidak ingin di ubah</i></small>
                                @error('foto')
                                    <small class="text-danger">{{ $message }}</small>
                                @enderror
                            </div>
                            <div class="float-right">
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                <button type="submit" class="btn btn-primary">Save changes</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    @endforeach
@endsection
