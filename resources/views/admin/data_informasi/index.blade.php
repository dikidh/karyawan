@extends('template_admin.app')

@section('title', 'Data Informasi')

@section('content')
    <section class="section">
        <div class="section-header">
            <h1>Data Informasi</h1>
        </div>
        <div class="section-body">
            <div class="card">
                <div class="card-body">
                    <a href="#" class="btn btn-primary" data-toggle="modal" data-target="#exampleModalTambah">Tambah Data</a>
                    <hr>
                    @if (session('informasi'))
                        <div class="alert alert-danger alert-dismissible">
                            <button type="button" class="close" data-dismiss="alert">&times;</button>
                            <strong>{{ session('informasi') }}</strong>
                        </div>
                    @endif
                    <table class="table table-bordered table-striped" id="myTable">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Informasi</th>
                                <th>Jenis Informasi</th>
                                <th>Status</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @if (!empty($informasis))
                                @foreach ($informasis as $no => $item)
                                    <tr>
                                        <td>{{ $no + 1 }}</td>
                                        <td>{{ $item->informasi }}</td>
                                        <td>{{ $item->jenis_informasi }}</td>
                                        <td>{{ $item->status }}</td>
                                        <td>
                                            <form action="{{ route('informasi.destroy', $item->id) }}" method="post">
                                                @method('DELETE')
                                                @csrf
                                                <a href="#" data-toggle="modal"
                                                    data-target="#exampleModalEdit{{ $item->id }}"
                                                    class="btn btn-sm btn-info"><i class="fas fa-edit"></i></a>
                                                <button type="submit" class="btn btn-danger btn-sm"><i
                                                        class="fas fa-trash"></i></button>
                                            </form>
                                        </td>
                                    </tr>
                                @endforeach
                            @endif
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </section>

    <!-- Button trigger modal -->
    {{-- <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal">
        Launch demo modal
    </button> --}}

    <!-- Modal Tambah-->
    <div class="modal fade" id="exampleModalTambah" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
        aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Tambah Data</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form action="{{ route('informasi.store') }}" method="post">
                        @csrf
                        <div class="form-group">
                            <label for="">Informasi</label>
                            <textarea name="informasi" id="" cols="30" rows="10" class="form-control"></textarea>
                            @error('informasi')
                                <small class="text-danger">{{ $message }}</small>
                            @enderror
                        </div>
                        <div class="form-group">
                            <label for="">Jenis Informasi</label>
                            <select name="jenis_informasi" id="" class="form-control">
                                <option value=" ">-- PIlihan --</option>
                                <option value="Umum">Umum</option>
                                <option value="Khusus">Khusus</option>
                            </select>
                            @error('jenis_informasi')
                                <small class="text-danger">{{ $message }}</small>
                            @enderror
                        </div>
                        <div class="form-group">
                            <label for="">Status</label>
                            <select name="status" id="" class="form-control">
                                <option value=" ">-- PIlihan --</option>
                                <option value="1">Aktif</option>
                                <option value="2">Tidak Aktif</option>
                            </select>
                            @error('jenis_informasi')
                                <small class="text-danger">{{ $message }}</small>
                            @enderror
                        </div>
                        <div class="float-right">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Keluar</button>
                            <button type="submit" class="btn btn-primary">Simpan</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <!-- Modal Edit-->
    @foreach ($informasis as $item)
        <div class="modal fade" id="exampleModalEdit{{ $item->id }}" tabindex="-1" role="dialog"
            aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Ubah Data</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="card-body">
                        <form action="{{ route('informasi.update', $item->id) }}" method="post">
                            @method('PUT')
                            @csrf
                            <div class="form-group">
                                <label for="">Informasi</label>
                                <textarea name="informasi" class="form-control" rows="10">{{ $item->informasi }}</textarea>
                                @error('informasi')
                                    <small class="text-danger">{{ $message }}</small>
                                @enderror
                            </div>
                            <div class="form-group">
                                <label for="">Jenis Informasi</label>
                                <select name="jenis_informasi" id="" class="form-control">
                                    <option value="Umum" {{ $item->jenis_informasi == 'Umum' ? 'selected' : '' }}>Umum
                                    </option>
                                    <option value="Khusus" {{ $item->jenis_informasi == 'Khusus' ? 'selected' : '' }}>
                                        Khusus</option>
                                </select>
                                @error('jenis_informasi')
                                    <small class="text-danger">{{ $message }}</small>
                                @enderror
                            </div>
                            <div class="form-group">
                                <label for="">Status</label>
                                <select name="status" id="" class="form-control">
                                    <option value="1" {{ $item->status == '1' ? 'selected' : '' }}>
                                        Aktif</option>
                                    <option value="2" {{ $item->status == '2' ? 'selected' : '' }}>Tidak Aktif
                                    </option>
                                </select>
                                @error('status')
                                    <small class="text-danger">{{ $message }}</small>
                                @enderror
                            </div>
                            <div class="float-right">
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Keluar</button>
                                <button type="submit" class="btn btn-primary">Simpan</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    @endforeach
@endsection
