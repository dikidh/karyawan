@extends('template_admin.app')

@section('title', 'Data Kegiatan')

@section('content')
    <section class="section">
        <div class="section-header">
            <h1>Data Kegiatan</h1>
        </div>
        <div class="section-body">
            <div class="card">
                <div class="card-body">
                    <a href="#" class="btn btn-primary" data-toggle="modal" data-target="#exampleModalTambah">Tambah Data</a>
                    <hr>
                    @if (session('repositori'))
                        <div class="alert alert-success alert-dismissible">
                            <button type="button" class="close" data-dismiss="alert">&times;</button>
                            <strong>{{ session('repositori') }}</strong>
                        </div>
                    @endif
                    <table class="table table-bordered table-striped" id="myTable">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Nama Kegiatan</th>
                                <th>Karyawan</th>
                                <th>Status Kegiatan</th>
                                <th>Hari & Tanggal</th>
                                <th>Jam</th>
                                <th>Keterangan</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @if (!empty($kegiatans))
                                @foreach ($kegiatans as $no => $item)
                                    <tr>
                                        <td>{{ $no + 1 }}</td>
                                        <td>{{ $item->nama_kegiatan }}</td>
                                        <td>{{ $item->karyawan->user->name }}</td>
                                        <td>
                                            @if ($item->status_kegiatan->id == 1)
                                                <span
                                                    class="badge badge-success">{{ $item->status_kegiatan->status_kegiatan }}</span>
                                            @elseif($item->status_kegiatan->id == 2)
                                                <span
                                                    class="badge badge-warning">{{ $item->status_kegiatan->status_kegiatan }}</span>
                                            @elseif($item->status_kegiatan->id == 3)
                                                <span
                                                    class="badge badge-danger">{{ $item->status_kegiatan->status_kegiatan }}</span>
                                            @endif
                                        </td>
                                        <td>{{ $item->hari }} {{ $item->tanggal }}</td>
                                        <td>{{ $item->jam }}</td>
                                        <td>{{ $item->keterangan }}</td>
                                        <td>
                                            <form action="{{ route('kegiatan.destroy', $item->id) }}" method="post">
                                                @method('DELETE')
                                                @csrf
                                                <a href="#" data-toggle="modal"
                                                    data-target="#exampleModalEdit{{ $item->id }}"
                                                    class="btn btn-sm btn-info"><i class="fas fa-edit"></i></a>
                                                <button type="submit" class="btn btn-danger btn-sm"><i
                                                        class="fas fa-trash"></i></button>
                                            </form>
                                        </td>
                                    </tr>
                                @endforeach
                            @endif
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </section>

    <!-- Button trigger modal -->
    {{-- <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal">
        Launch demo modal
    </button> --}}

    <!-- Modal Tambah-->
    <div class="modal fade" id="exampleModalTambah" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
        aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Tambah Data</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form action="{{ route('kegiatan.store') }}" method="post">
                        @csrf
                        <div class="form-group">
                            <label for="">Nama Kegiatan</label>
                            <input type="text" class="form-control" name="nama_kegiatan" placeholder="Masukan Nama">
                            @error('nama_kegiatan')
                                <small class="text-danger">{{ $message }}</small>
                            @enderror
                        </div>
                        <div class="form-group">
                            <label for="">Karyawan</label>
                            <select name="karyawan_id" id="" class="form-control">
                                <option value="">-- Pilihan --</option>
                                @foreach ($users as $item)
                                    <option value="{{ $item->id }}">{{ $item->user->name }}</option>
                                @endforeach
                            </select>
                            @error('karyawan_id')
                                <small class="text-danger">{{ $message }}</small>
                            @enderror
                        </div>
                        <div class="form-group">
                            <label for="">Status Kegiatan</label>
                            <select name="status_kegiatan_id" id="" class="form-control">
                                <option value="">-- Pilihan --</option>
                                @foreach ($status_kegiatans as $item)
                                    <option value="{{ $item->id }}">{{ $item->status_kegiatan }}</option>
                                @endforeach
                            </select>
                            @error('status_kegiatan_id')
                                <small class="text-danger">{{ $message }}</small>
                            @enderror
                        </div>
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="">Tanggal</label>
                                    <input type="date" class="form-control" name="tanggal">
                                </div>
                                @error('tanggal')
                                    <small class="text-danger">{{ $message }}</small>
                                @enderror
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="">Hari</label>
                                    <input type="text" class="form-control" name="hari" placeholder="Masukan Hari">
                                </div>
                                @error('hari')
                                    <small class="text-danger">{{ $message }}</small>
                                @enderror
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="">Jam</label>
                                    <input type="time" class="form-control" name="jam" placeholder="Masukan Jam">
                                </div>
                                @error('jam')
                                    <small class="text-danger">{{ $message }}</small>
                                @enderror
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="">Keterangan</label>
                            <textarea name="keterangan" rows="5" class="form-control"></textarea>
                            @error('keterangan')
                                <small class="text-danger">{{ $message }}</small>
                            @enderror
                        </div>
                        <div class="float-right">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                            <button type="submit" class="btn btn-primary">Simpan</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <!-- Modal Edit-->
    @foreach ($kegiatans as $item)
        <div class="modal fade" id="exampleModalEdit{{ $item->id }}" tabindex="-1" role="dialog"
            aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Ubah Data</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <form action="{{ route('kegiatan.update', $item->id) }}" method="post">
                            @method('PUT')
                            @csrf
                            <div class="form-group">
                                <label for="">Nama Kegiatan</label>
                                <input type="text" class="form-control" name="nama_kegiatan"
                                    placeholder="Masukan Nama" value="{{ $item->nama_kegiatan }}">
                                @error('nama_kegiatan')
                                    <small class="text-danger">{{ $message }}</small>
                                @enderror
                            </div>
                            <div class="form-group">
                                <label for="">Karyawan</label>
                                <select name="karyawan_id" id="" class="form-control">
                                    <option value="">-- Pilihan --</option>
                                    @foreach ($users as $user)
                                        <option value="{{ $user->id }}"
                                            {{ $user->id == $item->karyawan_id ? 'selected' : '' }}>{{ $user->name }}
                                        </option>
                                    @endforeach
                                </select>
                                @error('karyawan_id')
                                    <small class="text-danger">{{ $message }}</small>
                                @enderror
                            </div>
                            <div class="form-group">
                                <label for="">Status Kegiatan</label>
                                <select name="status_kegiatan_id" id="" class="form-control">
                                    <option value="">-- Pilihan --</option>
                                    @foreach ($status_kegiatans as $status_kegiatan)
                                        <option value="{{ $status_kegiatan->id }}"
                                            {{ $status_kegiatan->id == $item->status_kegiatan_id ? 'selected' : '' }}>
                                            {{ $status_kegiatan->status_kegiatan }}</option>
                                    @endforeach
                                </select>
                                @error('status_kegiatan_id')
                                    <small class="text-danger">{{ $message }}</small>
                                @enderror
                            </div>
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="">Tanggal</label>
                                        <input type="date" class="form-control" name="tanggal"
                                            value="{{ $item->tanggal }}">
                                    </div>
                                    @error('tanggal')
                                        <small class="text-danger">{{ $message }}</small>
                                    @enderror
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="">Hari</label>
                                        <input type="text" class="form-control" name="hari"
                                            placeholder="Masukan Hari" value="{{ $item->hari }}">
                                    </div>
                                    @error('hari')
                                        <small class="text-danger">{{ $message }}</small>
                                    @enderror
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="">Jam</label>
                                        <input type="time" class="form-control" name="jam"
                                            placeholder="Masukan Jam" value="{{ $item->jam }}">
                                    </div>
                                    @error('jam')
                                        <small class="text-danger">{{ $message }}</small>
                                    @enderror
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="">Keterangan</label>
                                <textarea name="keterangan" rows="5" class="form-control">{{ $item->keterangan ?? '' }}</textarea>
                                @error('keterangan')
                                    <small class="text-danger">{{ $message }}</small>
                                @enderror
                            </div>
                            <div class="float-right">
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                                <button type="submit" class="btn btn-primary">Simpan</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    @endforeach
@endsection
