@extends('template_admin.app')

@section('title', 'Data Profil')

@section('content')
    <section class="section">
        <div class="section-header">
            @include('sweetalert::alert')
            <h1>@yield('title')</h1>
        </div>

        <div class="section-body">
            <div class="card">
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-4">
                            <img alt="image" src="{{ url(Storage::url($user->foto)) }}" width="100%">
                            <form action="{{ route('ubah.profile.admin', $user->id) }}" method="post" enctype="multipart/form-data">
                                @method('put')
                                @csrf
                                <div class="form-group">
                                    <label for="">Foto</label>
                                    <input type="file" class="form-control" name="foto">
                                </div>
                                <button type="submit" class="btn btn-info btn sm">Ubah</button>
                            </form>
                        </div>
                        <div class="col-md-8">
                            <table class="table">
                                <tr>
                                    <th>Nama</th>
                                    <td>{{ $user->name }}</td>
                                </tr>
                                <tr>
                                    <th>Email</th>
                                    <td>{{ $user->email }}</td>
                                </tr>
                                <tr>
                                    <th>Status</th>
                                    <td>
                                        @if ($user->status == 1)
                                            <small class="badge badge-info">Aktif</small>
                                        @else
                                            <small class="badge badge-danger">Tidak Aktif</small>
                                        @endif
                                    </td>
                                </tr>
                                <tr>
                                    <th>Posisi</th>
                                    <td>
                                        @if ($user->role_user == 1)
                                            <small class="badge badge-success">Admin</small>
                                        @else
                                            <small class="badge badge-danger">Pekerja</small>
                                        @endif
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
    </section>
@endsection
