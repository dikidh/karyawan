@extends('template_admin.app')

@section('title', 'Dashboard')

@section('content')
    <div class="card">
        <div class="card-body">
            <h3>Dashboard Admin</h3>
        </div>
    </div>
    <div class="card">
        <div class="card-body">
            <div class="row">
                <div class="col-lg-3 col-md-6 col-sm-6 col-12">
                    <div class="card card-statistic-1">
                        <div class="card-icon bg-primary">
                            <i class="far fa-user"></i>
                        </div>
                        <div class="card-wrap">
                            <div class="card-header">
                                <h4>Total User</h4>
                            </div>
                            <div class="card-body">
                                {{ count($karyawan) }}
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6 col-sm-6 col-12">
                    <div class="card card-statistic-1">
                        <div class="card-icon bg-danger">
                            <i class="far fa-newspaper"></i>
                        </div>
                        <div class="card-wrap">
                            <div class="card-header">
                                <h4>Pekerja</h4>
                            </div>
                            <div class="card-body">
                                {{ count($karyawan) }}
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6 col-sm-6 col-12">
                    <div class="card card-statistic-1">
                        <div class="card-icon bg-warning">
                            <i class="far fa-file"></i>
                        </div>
                        <div class="card-wrap">
                            <div class="card-header">
                                <h4>Inforamsi</h4>
                            </div>
                            <div class="card-body">
                                {{ count($informasi) }}
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6 col-sm-6 col-12">
                    <div class="card card-statistic-1">
                        <div class="card-icon bg-success">
                            <i class="fas fa-circle"></i>
                        </div>
                        <div class="card-wrap">
                            <div class="card-header">
                                <h4>Kegiatan</h4>
                            </div>
                            <div class="card-body">
                                {{ count($kegiatan) }}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="card">
        <div class="card-body">
            <div class="row">
                <div class="col-md-4">
                    <img src="../image/logo_250.png" alt="" width="90%">
                </div>
                <div class="col-md-8">
                    <h5>Cv Samudera Biru Nusantara</h5>
                    <p>
                        Perusahaan kontraktor umum dibidang jasa Konstruksi, Pembangunan Batching Plant,
                        Pembongkaran Batching Plant, Asphalt Mixing Plant, A Stone Crusher Plant, Pembongkaran Alat,
                        Electrical , Comissioning, Ericcson, Instalasi Air. Kami memiliki Tim manajemen yang
                        berpengalaman dan juga didukung oleh staff ahli yang professional dan tim pelaksana proyek
                        yang mahir terampil dan handal, mengutamakan mutu, ketepatan waktu dalam proyek konstruksi
                        komersial di Indonesia.
                    </p>
                </div>
            </div>
        </div>
    </div>
@endsection
