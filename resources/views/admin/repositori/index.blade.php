@extends('template_admin.app')

@section('title', 'Repositori')

@section('content')
    <div class="card">
        <div class="card-body">
            <a href="" class="btn btn-primary">Tambah Data Data</a>
            <hr>
            @if (session('repositori'))
                <div class="alert alert-success alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert">&times;</button>
                    <strong>{{ session('repositori') }}</strong>
                </div>
            @endif
            <table class="table table-bordered table-striped" id="myTable">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Status Masa Berlaku</th>
                        <th>Tanggal Awal</th>
                        <th>Tanggal Akhir</th>
                        <th>Dokumen</th>
                        <th>Status Jenis Kegiatan</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    @if (!empty($repositori))
                        @foreach ($repositori as $no => $item)
                            <tr>
                                <td>{{ $no + 1 }}</td>
                                <td>{{ $item->status_masa_berlaku }}</td>
                                <td>{{ $item->tanggal_awal }}</td>
                                <td>{{ $item->tanggal_akhir }}</td>
                                <td>
                                    @php
                                        $str = $item->dokumen;
                                        $ex = explode('.', $str);
                                        // dd($ex);
                                    @endphp
                                    @if ($ex[1] == 'pdf')
                                        <a href="{{ url('uploads/' . $item->dokumen) }}">
                                            <img src="{{ url('files/pdf.jpg') }}" alt="" width="50">
                                        </a>
                                    @elseif($ex[1] == 'docx' || $ex[1] == 'doc')
                                        <a href="{{ url('uploads/' . $item->dokumen) }}">
                                            <img src="{{ url('files/word.png') }}" alt="" width="50">
                                        </a>
                                    @elseif($ex[1] == 'png' || $ex[1] == 'jpg' || $ex[1] == 'jpeg')
                                        <a href="{{ url('uploads/' . $item->dokumen) }}">
                                            <img src="{{ url('files/picture.png') }}" alt="" width="50">
                                        </a>
                                    @endif
                                </td>
                                <td>{{ $item->status_jenis_dokumen }}</td>
                                {{-- <td>{{ $item->no_dokumen }}</td>
                            <td>{{ $item->judul_kerjasama }}</td>
                            <td>{{ $item->deskripsi }}</td>
                            <td>{{ $item->nama_instansi }}</td>
                            <td>{{ $item->alamat }}</td>
                            <td>{{ $item->nama }}</td>
                            <td>{{ $item->jabatan }}</td> --}}
                                <th>
                                    <a href="{{ url('edit_repositori/' . $item->id) }}" class="btn btn-sm btn-info"><i
                                            class="fas fa-edit"></i></a>
                                    <a href="{{ url('hapus_repositori/' . $item->id) }}" class="btn btn-danger btn-sm"><i
                                            class="fas fa-trash"></i></a>
                                </th>
                            </tr>
                        @endforeach
                    @endif
                </tbody>
            </table>
        </div>
    </div>
@endsection
