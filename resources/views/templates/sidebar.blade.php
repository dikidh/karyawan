<div class="main-sidebar">
    <aside id="sidebar-wrapper">
        <div class="sidebar-brand mt-3 mb-2">
            <a href="index.html">
                <img src="../image/logo_250.png" alt="" width="90">
            </a>
        </div>
        <div class="sidebar-brand sidebar-brand-sm">
            <a href="index.html">St</a>
        </div>
        <ul class="sidebar-menu">
            <li class="menu-header">Dashboard</li>
            <li class="{{ request()->is('dashboard*') ? 'active' : '' }}"><a class="nav-link"
                    href="{{ route('dashboard') }}"><i class="fas fa-fire"></i>
                    <span>Dashboard</span></a></li>
            <li class="menu-header">Menu</li>
            @if (auth()->user()->role != 1)
                <li class="{{ request()->is('agenda*') ? 'active' : '' }}"><a class="nav-link"
                        href="{{ route('agenda') }}"><i class="fas fa-university"></i>
                        <span>
                            Data Kegiatan</span></a></li>
            @endif
            <li class="{{ request()->is('informasi*') ? 'active' : '' }}"><a class="nav-link"
                    href="{{ route('informasi-pekerja') }}"><i class="fas fa-file"></i>
                    <span>
                        Data Informasi </span></a></li>

        </ul>

        <div class="mt-4 mb-4 p-3 hide-sidebar-mini">
            <a href="{{ route('logout') }}" class="btn btn-primary btn-lg btn-block btn-icon-split">
                <i class="fas fa-sign-out-alt"></i> Logout
            </a>
        </div>
    </aside>
</div>
