<div class="main-sidebar">
    <aside id="sidebar-wrapper">
        <div class="sidebar-brand">
            <a href="index.html">Admin</a>
        </div>
        <div class="sidebar-brand sidebar-brand-sm">
            <a href="index.html">A</a>
        </div>
        <ul class="sidebar-menu">
            <li class="menu-header">Dashboard</li>
            <li class="{{ request()->is('dashboard_admin') ? 'active' : '' }}"><a class="nav-link"
                    href="{{ route('dashboard_admin') }}"><i class="fas fa-fire"></i>
                    <span>Dashboard</span></a></li>
            <li class="menu-header">Starter</li>
            <li class="{{ request()->is('karyawan*') ? 'active' : '' }}"><a class="nav-link"
                    href="{{ route('karyawan.index') }}"><i class="fas fa-users"></i>
                    <span>Data Karyawan</span></a></li>
            <li class="{{ request()->is('kegiatan*') ? 'active' : '' }}"><a class="nav-link"
                    href="{{ route('kegiatan.index') }}"><i class="fas fa-calendar-alt"></i>
                    <span>Data Kegiatan</span></a></li>
            {{-- <li class="{{ request()->is('status_kegiata*') ? 'active' : '' }}"><a class="nav-link"
                    href="{{ route('status_kegiatan.index') }}"><i class="fas fa-university"></i>
                    <span>Data Status Kegiatan</span></a></li> --}}
            <li class="{{ request()->is('informasi*') ? 'active' : '' }}"><a class="nav-link"
                    href="{{ route('informasi.index') }}"><i class="far fa-clipboard"></i>
                    <span>Data Informasi</span></a></li>
            <li class="{{ request()->is('user*') ? 'active' : '' }}"><a class="nav-link"
                    href="{{ route('user.index') }}"><i class="fas fa-user"></i>
                    <span>Data User</span></a></li>
            <li class="{{ request()->is('laporan*') ? 'active' : '' }}"><a class="nav-link"
                    href="{{ route('laporan.index') }}"><i class="fas fa-file"></i>
                    <span>Laporan</span></a></li>
            {{-- <li class="nav-item dropdown">
            <a href="#" class="nav-link has-dropdown"><i class="fas fa-american-sign-language-interpreting"></i> <span>Kerjasama</span></a>
            <ul class="dropdown-menu">
              <li><a href="{{ route('repositori_admin') }}">Repositori</a></li>
              <li><a href="gmaps-draggable-marker.html">Program</a></li>
              <li><a href="gmaps-geocoding.html">Mitra</a></li>
            </ul> --}}
        </ul>

        <div class="mt-4 mb-4 p-3 hide-sidebar-mini">
            <a href="{{ route('logout') }}" class="btn btn-primary btn-lg btn-block btn-icon-split">
                <i class="fas fa-sign-out-alt"></i> Logout
            </a>
        </div>
    </aside>
</div>
