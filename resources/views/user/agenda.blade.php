@extends('templates.app')

@section('title', 'Data Kegiatan')

@section('content')
    <section class="section">
        <div class="section-header">
            @include('sweetalert::alert')
            <h1>@yield('title')</h1>
        </div>

        <div class="section-body">
            <div class="card">
                <div class="card-body">
                    <a href="{{ route('cetakKegiatan') }}" class="btn btn-info">Cetak Kegiatan</a>
                    <hr>
                    <table class="table table-bordered table-striped" id="myTable">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Nama Kegiatan</th>
                                <th>Karyawan</th>
                                <th>Status Kegiatan</th>
                                <th>Hari & Tanggal</th>
                                <th>Jam</th>
                                <th>Keterangan</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @if (!empty($kegiatans))
                                @foreach ($kegiatans as $no => $item)
                                    <tr>
                                        <td>{{ $no + 1 }}</td>
                                        <td>{{ $item->nama_kegiatan }}</td>
                                        <td>{{ $item->karyawan->user->name ?? '' }}</td>
                                        <td>
                                            @if ($item->status_kegiatan->id == 1)
                                                <span
                                                    class="badge badge-success">{{ $item->status_kegiatan->status_kegiatan }}</span>
                                            @elseif($item->status_kegiatan->id == 2)
                                                <span
                                                    class="badge badge-warning">{{ $item->status_kegiatan->status_kegiatan }}</span>
                                            @elseif($item->status_kegiatan->id == 3)
                                                <span
                                                    class="badge badge-danger">{{ $item->status_kegiatan->status_kegiatan }}</span>
                                            @endif
                                        </td>
                                        <td>{{ $item->hari }} {{ $item->tanggal }}</td>
                                        <td>{{ $item->jam }}</td>
                                        <td>{{ $item->keterangan }}</td>
                                        <td>
                                            <a href="#" data-toggle="modal"
                                                data-target="#exampleModalDetail{{ $item->id }}"
                                                class="btn btn-sm btn-info"><i class="fas fa-eye"></i></a>
                                                <a href="{{ route('cetakKegiatanId', $item->id) }}" class="btn btn-sm btn-danger"><i class="fas fa-print"></i></a>
                                        </td>
                                    </tr>
                                @endforeach
                            @endif
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </section>

    @foreach ($kegiatans as $item)
        <div class="modal fade" id="exampleModalDetail{{ $item->id }}" tabindex="-1" role="dialog"
            aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Detail Data Kegiatan</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <h5 class="text-center">{{ $item->nama_kegiatan }}</h5>
                        <div class="row">

                        </div>
                    </div>
                </div>
            </div>
        </div>
    @endforeach
@endsection
