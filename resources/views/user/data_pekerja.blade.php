@extends('templates.app')

@section('title', 'Data Pekerja')

@section('content')
    <section class="section">
        <div class="section-header">
            @include('sweetalert::alert')
            <h1>@yield('title')</h1>
        </div>

        <div class="section-body">
            <div class="card">
                <div class="card-body">
                    <table class="table table-bordered table-striped" id="myTable">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Foto</th>
                                <th>Nama</th>
                                <th>Posisi</th>
                                <th>Jenis Kelamin</th>
                                <th>No Telepon</th>
                                <th>Alamat</th>
                            </tr>
                        </thead>
                        <tbody>
                            @if (!empty($pekerja))
                                @foreach ($pekerja as $no => $item)
                                    <tr>
                                        <td>{{ $no + 1 }}</td>
                                        <td>
                                            @if ($item->user->foto)
                                                <img src="{{ url(Storage::url($item->user->foto)) }}" alt=""
                                                    width="50">
                                            @else
                                                <small>kososng</small>
                                            @endif
                                        </td>
                                        <td>{{ $item->user->name ?? '' }}</td>
                                        <td>{{ $item->posisi }}</td>
                                        <td>{{ $item->jenis_kelamin }}</td>
                                        <td>{{ $item->no_telepon }}</td>
                                        <td>{{ $item->alamat }}</td>
                                    </tr>
                                @endforeach
                            @endif
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </section>
@endsection
