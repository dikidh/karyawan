@extends('templates.app')

@section('title', 'Data Informasi')

@section('content')
    <section class="section">
        <div class="section-header">
            @include('sweetalert::alert')
            <h1>@yield('title')</h1>
        </div>

        <div class="section-body">
            <div class="card">
                <div class="card-body">
                    <table class="table table-bordered table-striped" id="myTable">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Informasi</th>
                                <th>Jenis Informasi</th>
                                <th>Status</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @if (!empty($informasis))
                                @foreach ($informasis as $no => $item)
                                    <tr>
                                        <td>{{ $no + 1 }}</td>
                                        <td>{{ $item->informasi }}</td>
                                        <td>{{ $item->jenis_informasi }}</td>
                                        <td>
                                            @if ($item->status == 1)
                                                <small class="badge badge-success">Aktif</small>
                                            @else
                                                <small class="badge badge-danger">Tidak Aktif</small>
                                            @endif
                                        </td>
                                        <td>
                                            <a href="#" data-toggle="modal"
                                                data-target="#exampleModalDetail{{ $item->id }}"
                                                class="btn btn-sm btn-info"><i class="fas fa-eye"></i></a>
                                        </td>
                                    </tr>
                                @endforeach
                            @endif
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </section>

    @foreach ($informasis as $item)
        <div class="modal fade" id="exampleModalDetail{{ $item->id }}" tabindex="-1" role="dialog"
            aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Detail Informasi</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <h5 class="text-center">{{ $item->nama_kegiatan }}</h5>
                        <small>{{ $item->jenis_informasi }}</small>
                        <div class="row mt-3">
                            <div class="col">
                                {{ $item->informasi }}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    @endforeach
@endsection
