@extends('templates.app')

@section('title', 'Dashboard')

@section('content')
    <section class="section">
        <div class="section-header">
            @include('sweetalert::alert')
            <h1>@yield('title')</h1>
        </div>

        <div class="section-body">
            <div class="card">
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-4">
                            <img src="../image/logo_250.png" alt="" width="100%">
                        </div>
                        <div class="col-md-8">
                            <h5>Cv Samudera Biru Nusantara</h5>
                            <p>
                                Perusahaan kontraktor umum dibidang jasa Konstruksi, Pembangunan Batching Plant,
                                Pembongkaran Batching Plant, Asphalt Mixing Plant, A Stone Crusher Plant, Pembongkaran Alat,
                                Electrical , Comissioning, Ericcson, Instalasi Air. Kami memiliki Tim manajemen yang
                                berpengalaman dan juga didukung oleh staff ahli yang professional dan tim pelaksana proyek
                                yang mahir terampil dan handal, mengutamakan mutu, ketepatan waktu dalam proyek konstruksi
                                komersial di Indonesia.
                            </p>
                        </div>
                    </div>
                    <div class="row mt-3">
                        <div class="col-md-6">
                            <h5>Visi</h5>
                            <p>
                                Samudera Biru Nusantara-Menjadi Perusahaan kontruksi unggul Dan inovatif,
                                berintegrasi tinggi dengan karya yang berkualitas.
                            </p>
                        </div>
                        <div class="col-md-6">
                            <h5>Misi</h5>
                            <p>
                                1. Samudera Biru Nusantara-mengembangkan sumber daya manusia yang berkualitas,
                                professional, unggul dan prima dalam rangka meningkatkan nilai perusahaan.
                                2. Perusahaan yang selalu mengembangkan inovasi dan menerapkan quality control untuk
                                hasil karya bangunan yang berkualitas dan tepat waktu.
                                3. Meningkatkan ekspansi pasar untuk pengembangan usaha yang berkelanjutan.
                                4. Memiliki organisasi yang baik, transparan dan amanah dengan mengutamakan mutu dan
                                K3L.
                                5. Samudera Biru Nusantara bangga terhadap profesi dan hasil karya kami.
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
