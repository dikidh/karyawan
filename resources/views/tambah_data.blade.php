@extends('templates.app')

@section('title','Tambah Data')

@section('content')
    <div class="row">
        <div class="col-md-5">
            <form action="{{ route('tambah_data') }}" method="post" enctype="multipart/form-data">
                @csrf
            <div class="card">
                <div class="card-header">
                    <i class="fa fa-calendar"></i>  <strong class="ml-2">Masa Berlaki</strong>
                </div>
                <div class="card-body">
                    <div class="form-group">
                        <label for="">Status</label>
                        <select name="status_kerjasama" id="" class="form-control">
                            <option value="">Status Kerja Sama</option>
                            <option value="Aktif">Aktif</option>
                            <option value="Tidak Aktif">Tidak Aktif</option>
                        </select>
                        @error('status_kerjasama')
                            <small class="text-danger">{{ $message }}</small>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label for="">Tanggal Awal</label>
                        <input type="date" value="" name="tanggal_awal" class="form-control">
                        @error('tanggal_awal')
                            <small class="text-danger">{{ $message }}</small>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label for="">Tanggal Berakhir</label>
                        <input type="date" value="" name="tanggal_akhir" class="form-control">
                        @error('tanggal_akhir')
                            <small class="text-danger">{{ $message }}</small>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label for="">Dokumen</label>
                        <input class="form-control" name="dokumen" type="file" id="formFileDisabled" />
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-7">
            <div class="card">
                <div class="card-header">
                      <strong class="ml-2">Jenis Dokumen Kerjasama</strong>
                </div>
                <div class="card-body">
                    <div class="form-group">
                        <label for="">Status</label>
                        <select name="status_dokumen_kerjasama" id="" class="form-control">
                            <option value="Monitoring of Understanding (Mou)">Monitoring of Understanding (Mou)</option>
                            <option value="Monitoring of Arragangement (Mao)">Monitoring of Arragangement (Mao)</option>
                            <option value="Implementasi Arragangement (IA)">Implementasi Arragangement (IA)</option>
                        </select>
                        @error('status_dokumen_kerjasama')
                            <small class="text-danger">{{ $message }}</small>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label for="">Nomor Dokumen</label>
                        <input type="text" value="" name="no_dokumen" class="form-control">
                        @error('no_dokumen')
                            <small class="text-danger">{{ $message }}</small>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label for="">Judul Kerjasama</label>
                        <input type="text" value="" name="judul_dokumen" class="form-control">
                        @error('judul_dokumen')
                            <small class="text-danger">{{ $message }}</small>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label for="">Deskripsi</label><br>
                        <small>Ringkasan singkat terkait cakupan atau kegiatan kerjasama</small>
                        <textarea name="deskripsi" id="" rows="5" class="form-control"></textarea>
                        @error('deskripsi')
                            <small class="text-danger">{{ $message }}</small>
                        @enderror
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-5">
            <div class="card">
                <div class="card-header">
                    <i class="fa fa-calendar"></i>  <strong class="ml-2">Penggiat Kerja Sama</strong>
                </div>
                <div class="card-body">
                    <div class="form-group">
                        <label for="">Nama Instansi</label>
                        <select name="nama_instansi" id="" class="form-control">
                            <option value="Universitas Muhammadiyah Tangerang">Universitas Muhammadiyah Tangerang</option>
                            <option value="UBL">UBL</option>
                        </select>
                        @error('nama_instansi')
                            <small class="text-danger">{{ $message }}</small>
                        @enderror
                    </div>
                    {{-- <div class="form-group">
                        <label for="">Alamat</label>
                        <textarea class="form-control" name="alamat" rows="5"></textarea>
                    </div> --}}
                    <div class="row mt-3">
                        <div class="col-md-3">
                            <label for="">Alamat</label>
                        </div>
                        <div class="col-md-9">
                            <textarea name="alamat" id="" rows="10" class="form-control"></textarea>
                            @error('alamat')
                                <small class="text-danger">{{ $message }}</small>
                            @enderror
                        </div>
                    </div>
                    <hr>
                    <div class="form-group">
                        <label for="">Penandatanganan</label><br>
                        <small>Pejabat yang menandatangani dokumen</small>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <label for="">Nama :</label>
                            <input type="text" name="nama" class="form-control">
                            @error('nama')
                                <small class="text-danger">{{ $message }}</small>
                            @enderror
                        </div>
                        <div class="col-md-6">
                            <label for="">Jabatan :</label>
                            <input type="text" name="jabatan" class="form-control">
                            @error('jabatan')
                                <small class="text-danger">{{ $message }}</small>
                            @enderror
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-7">
            <div class="card">
                <div class="card-header">
                    <i class="fa fa-calendar"></i>  <strong class="ml-2">Bentuk Kegiatan</strong>
                </div>
                <div class="card-body height-100">
                        <div class="form-group">
                            <label for="">Status</label>
                            <select name="status_kegiatan" id="" class="form-control">
                                <option value="">Pililh Bentuk Kegiatan</option>
                                <option value="Aktif">Aktif</option>
                                <option value="Tidak Aktif">Tidak Aktif</option>
                            </select>
                            @error('status_kegiatan')
                                <small class="text-danger">{{ $message }}</small>
                            @enderror
                        </div>
                        <button type="submit" class="btn btn-success"><i class="fa fa-bars"></i> Daftar</button>
                        <button type="submit" class="btn btn-primary">Save</button>
                        <a href="{{ route('data') }}" class="btn btn-secondary">Back</a>
                    </div>
                </div>
            </div>
        </form>
        </div>
@endsection