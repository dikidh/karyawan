@extends('templates.app')

@section('title','Upload')

@section('content')
    <div class="card">
        <div class="card-body">
            <div class="row">
                <div class="col-md-6">
                    <table class="table table-bordered" id="myTable">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Nama</th>
                                <th>File</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($file as $no => $item)
                                <tr>
                                    <td>{{ $no+1 }}</td>
                                    <td>{{ $item->nama }}</td>
                                    {{-- <td>{{ $item->file }}</td> --}}
                                    <td>
                                        {{-- <img src="{{ url('uploads/'. $item->file) }}" width="100%" height="500px"> --}}
                                        {{-- <a href="{{ url('uploads/'. $item->file) }}" target="_blank" class="btn btn-sm btn-warning">File</a> --}}
                                        {{-- @if ($item->file->extension() == 'pdf')
                                            <small>pdf</small>
                                        @elseif($item->file->extension() == 'docx')
                                            <small>word</small>
                                        @endif --}}
                                        @php
                                           $str  = $item->file;
                                           $ex = (explode(".", $str));
                                        @endphp
                                        @if ($ex[1] == 'pdf')
                                           <a href="{{ url('uploads/'. $item->file) }}">
                                               <img src="{{ url('/files/pdf.jpg') }}" alt="" width="30">
                                           </a>
                                        @elseif($ex[1] == 'docx')
                                            <a href="{{ url('uploads/'. $item->file) }}">
                                                <img src="{{ url('/files/word.png') }}" alt="" width="30">
                                            </a>
                                        @else
                                            <small>Apa aja lah</small>
                                        @endif
                                    </td>
                                    <td>
                                        <a href="" class="btn btn-info btn-sm"><i class="fas fa-edit"></i></a>
                                        <a href="" class="btn btn-danger btn-sm"><i class="fas fa-trash"></i></a>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
                <div class="col-md-6">
                    <form action="{{ url('upload') }}" method="post" enctype="multipart/form-data">
                        @csrf
                        <div class="form-group">
                            <label for="">Nama</label>
                            <input type="text" class="form-control" name="nama">
                            @error('nama')
                                <small class="text-danger">{{ $message }}</small>
                            @enderror
                        </div>
                        <div class="form-group">
                            <label for="">File</label>
                            <input type="file" class="form-control" name="file">
                            @error('file')
                                <small class="text-danger">{{ $message }}</small>
                            @enderror
                        </div>
                        <button type="submit" class="btn btn-primary">Save</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection